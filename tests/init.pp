# @summary Test postgis with default parameters.
#
class { 'postgis':
  postgresql_version => '12',
  postgis_version    => '3.2',
}
