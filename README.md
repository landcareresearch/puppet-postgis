# postgis

[![Puppet Forge](http://img.shields.io/puppetforge/v/landcareresearch/postgis.svg)](https://forge.puppetlabs.com/landcaresearch/postgis)
[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_PuppetPostgis_Build%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_PuppetPostgis_Build&guest=1)

## About

Installs and configures a postgresql database with the PostGIS extension installed.
Note, this module uses the puppetlabs/postgresql puppet module.  It also uses the ppa:ubuntugis to install postgis.

## API

See REFERENCE.md

## Limitations

Works with debian and redhat based OS's.

## Development

The module is open source and available on bitbucket.  Please fork!
